package com.ysl.bonjour.controller;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.ysl.bonjour.bean.BonjourMakeupRequest;
import com.ysl.bonjour.bean.BonjourMakeupResponse;
import com.ysl.bonjour.bean.BonjourRequest;
import com.ysl.bonjour.bean.BonjourResponse;
import com.ysl.bonjour.core.BonjourRunner;
import com.ysl.bonjour.util.Constants;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * @author pku134
 *This class is to show all summary of the call.
 */
public class SummaryController implements Controller,Initializable {
	@FXML private Label summary;
	@FXML private Label product1;
	@FXML private Label product2;
	@FXML private Label tip1;
	@FXML private Label tip2;
	@FXML private Label tip3;
	@FXML private Label tip4;
	@FXML private Label emailID;
	@FXML private Button email;
	@FXML private Button back;
	@FXML private VBox summaryBody;
	private BorderPane pane;
	private Scene scene;
	private Logger log = Logger.getLogger(SummaryController.class);
	private static BonjourMakeupRequest request;
	private static BonjourMakeupResponse response;

	@FXML
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		log.info("DEBUG::Entered in to Intialize block of Summary Controller");
		response=getResponse();
		if (response!= null) {
			emailID.setText(response.getEmailID());
			product1.setText(response.getProduct1());
			product2.setText(response.getProduct2());
			tip1.setText(response.getTip1());
			tip2.setText(response.getTip2());
			tip3.setText(response.getTip3());
		} else {
			log.info("INFO::Could not receve input request to summary");
		}
	}

	@Override
	public Scene getScene(BonjourRequest request, BonjourResponse response) {

		try {
			log.info("DEBUG::In Controller of Summary Stage");
			setRequest((BonjourMakeupRequest) request);
			setResponse((BonjourMakeupResponse) response);
			FXMLLoader fxmlLoader=new FXMLLoader(SummaryController.class.getClassLoader().getResource(Constants.SUMMARY_FXML_PATH));
			pane = (BorderPane) fxmlLoader.load();
			if (pane != null) {
				pane.setBackground(new Background(new BackgroundFill(Color.BLACK, new CornerRadii(10), new Insets(5))));
				scene = new Scene(pane);
				((BorderPane) scene.getRoot()).getChildren().addAll();
				scene.setRoot(pane);
				return scene;
			} else {
				log.error("ERROR::An error due to receival of null pane");
			}
		} catch (Exception e) {
			log.error("ERROR::Error occured in getting Summary page", e);
			scene = null;
		}
		return scene;
	}

	@Override
	public void setScene(Scene scene, BonjourRequest request, BonjourResponse response) {
		try
		{
		if (scene != null) {
			this.scene = scene;
			log.info("DEBUG::Moving ahead to next scene as navigation initiated");
			scene.getWindow().hide();
			Controller controller = BonjourControllerFactory.getController("MakeoverController");
			if (controller != null) {
				BonjourRunner runner = new BonjourRunner();
				runner.execute(controller,request,response);		
			}
			else
			{
				log.info("INFO::Controller Object is null");				
			}		
		}
		else
		{
			log.info("INFO::Controller Scene is not avaialable");
		}
		}
		catch(Exception e)
		{
			log.error("ERROR::Could not start scene for MakeoverController from Userconroller",e);
		}

	}
	@FXML
	public void setBackMeetingList(ActionEvent event)
	{
		Node node=(Node)event.getSource();
		setScene(node.getScene(),request,response);
	}

	public BonjourMakeupRequest getRequest() {
		return request;
	}

	public void setRequest(BonjourMakeupRequest request) {
		this.request = request;
	}

	public BonjourMakeupResponse getResponse() {
		return response;
	}

	public void setResponse(BonjourMakeupResponse response) {
		this.response =  response;
	}
	
}
