package com.ysl.bonjour.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.ysl.bonjour.bean.BonjourMakeupRequest;
import com.ysl.bonjour.bean.BonjourMakeupResponse;
import com.ysl.bonjour.bean.BonjourRequest;
import com.ysl.bonjour.bean.BonjourResponse;
import com.ysl.bonjour.bean.MeetingProperties;
import com.ysl.bonjour.core.BonjourRunner;
import com.ysl.bonjour.util.Constants;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * @author pku134
 *
 *         This is for Meeting detailed Controller
 */
public class MakeupDetailedController implements Controller, Initializable {

	private Logger log = Logger.getLogger(MakeupDetailedController.class);
	private Scene scene;
	private BorderPane pane;
	private TableView<MeetingProperties> meetingDetailedTable = new TableView<>();
	@FXML private VBox meetingBody;
	@FXML private Button service;
	@FXML private MenuButton hangoutButton;
	@FXML private MenuButton videoButton;
	@FXML private TextField emailID;
	private BonjourMakeupRequest request=new BonjourMakeupRequest();
	private BonjourMakeupResponse response=new BonjourMakeupResponse();
	
	
	@Override
	public Scene getScene(BonjourRequest request,BonjourResponse response) {
		try {
			log.info("INFO::In makeover of Detailed Controller ");
			FXMLLoader fxmlLoader=new FXMLLoader(MakeoverListController.class.getClassLoader().getResource(Constants.MDETAILS_FXML_PATH));
			pane = (BorderPane) fxmlLoader.load();
			if (pane != null) 
			{
				setRequest((BonjourMakeupRequest)this.request);
				setResponse((BonjourMakeupResponse)this.response);
				pane.autosize();
				pane.setBackground(new Background(new BackgroundFill(Color.BLACK, new CornerRadii(10), new Insets(5))));
				scene = new Scene(pane);
				((Pane) scene.getRoot()).getChildren().addAll();
				scene.setRoot(pane);
				return scene;
			} else {
				log.error("ERROR::Error in getting Detailed controller");
			}
		} catch (Exception e) {
			log.error("ERROR::Error occured in getting hangout page", e);
			scene = null;
		}
		return scene;
	}

	@Override
	public void setScene(Scene scene,BonjourRequest request,BonjourResponse response) {
		try {
			if (scene != null) {
				log.info("DEBUG::Moving ahead to next scene as navigation initiated");
				this.scene = scene;
				this.response=(BonjourMakeupResponse)response;
				
				
				scene.getWindow().hide();
				Controller controller = BonjourControllerFactory.getController("ServiceController");
				if (controller != null) {
					BonjourRunner runner = new BonjourRunner();
					runner.execute(controller,request,response);
				} else {
					log.info("INFO::Controller Object is null");
				}
			} else {
				log.info("INFO::Controller Scene is not avaialable");
			}
		} catch (Exception e) {
			log.error("ERROR::Could not start scene for MakeoverController from Userconroller", e);
		}

	}

	@FXML
	public void setCall(ActionEvent event) {

		Node node1 = (Node) event.getSource();
		request=getRequest();
		response=getResponse();
		response.setEmailID(emailID.getText());
		setScene(node1.getScene(),request,response);

	}
	private TableView<MeetingProperties> getMeetingDetails(String meetingID) {

		TableView<MeetingProperties> meetingTable = new TableView<>();
		meetingTable.setEditable(true);
		List<MeetingProperties> list = new ArrayList<>();
		list.add(new MeetingProperties("User", Constants.USER_NAME));
		list.add(new MeetingProperties("Country", Constants.COUNTRY));
		list.add(new MeetingProperties("UserID", "john.doe@gmail.com"));
		list.add(new MeetingProperties("Call Provider", "Hangout"));
		list.add(new MeetingProperties("Expect for ", "A look"));
		list.add(new MeetingProperties("Details", "Smokey eyes"));
		ObservableList<MeetingProperties> items = FXCollections.observableList(list);
		TableColumn<MeetingProperties, String> keyName = new TableColumn<>("Meeting");
		keyName.setCellValueFactory(new PropertyValueFactory<>("propertiesKey"));
		keyName.setMinWidth(180);
		TableColumn<MeetingProperties, String> valueName = new TableColumn<>((new Date().toString()).substring(11, 16)+" to "+new Integer((Integer.parseInt((new Date().toString()).substring(11,13)))+1)+":00");
		valueName.setCellValueFactory(new PropertyValueFactory<>("propertiesValue"));
		valueName.setMinWidth(180);
		meetingTable.autosize();
		meetingTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		meetingTable.setFixedCellSize(40);
		meetingTable.setItems(items);
		meetingTable.getColumns().addAll(keyName, valueName);
		return meetingTable;
	}

	@FXML
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) 
	{
		log.info("DEBUG:: Instialize Table data for Meeting details");
		this.meetingDetailedTable=getMeetingDetails("12345");
		if (meetingDetailedTable != null) {			
			meetingBody.getChildren().add(meetingDetailedTable);
			VBox.setMargin(meetingDetailedTable, new Insets(20));
			hangoutButton.toFront();
			videoButton.toFront();
			emailID.toFront();
			service.toFront();
		}
		else
		{
			log.info("INFO::Inside Instializable Table data not receved for Meeting details");
		}		
	}

	public BonjourMakeupRequest getRequest() {
		return request;
	}

	public void setRequest(BonjourMakeupRequest request) {
		this.request = request;
	}

	public BonjourMakeupResponse getResponse() {
		return response;
	}

	public void setResponse(BonjourMakeupResponse response) {
		this.response = response;		
	}

}
