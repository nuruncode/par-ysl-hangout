package com.ysl.bonjour.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.ysl.bonjour.bean.BonjourRequest;
import com.ysl.bonjour.bean.BonjourResponse;
import com.ysl.bonjour.bean.MakeupIndicator;
import com.ysl.bonjour.core.BonjourRunner;
import com.ysl.bonjour.util.Constants;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;

/**
 * @author pku134
 *
 *         This class refers to meeting list details.
 */
public class MakeoverListController implements Controller {

	@FXML
	private Hyperlink joinMeeting;
	private TableView<MakeupIndicator> meetingListTable = new TableView<>();
	private Logger log = Logger.getLogger(MakeoverListController.class);
	private BorderPane pane;
	private Scene scene;

	@Override
	public Scene getScene(BonjourRequest request,BonjourResponse response) {
		try {
			log.info("INFO::In Controller of Makeover controller");
			FXMLLoader fxmlLoader=new FXMLLoader(MakeoverListController.class.getClassLoader().getResource(Constants.MAKUP_FXML_PATH));
			pane = (BorderPane) fxmlLoader.load();
			if (pane != null) {
				pane.setBackground(new Background(new BackgroundFill(Color.BLACK, new CornerRadii(10), new Insets(5))));
				log.info("DEBUG::entered in to Setting Frame for Pane at MakeoverController");
				scene = new Scene(pane);
				this.meetingListTable = addMeetingList(this.meetingListTable);

				if (meetingListTable != null) {
					pane.setCenter(meetingListTable);
					((BorderPane) scene.getRoot()).getChildren().addAll();
				}
				scene.setRoot(pane);
			} else {
				log.error("ERROR::Parent for MakeoverList controller could not loaded");

			}
		} catch (Exception e) {
			log.error("ERROR::Error occured in getting Makeover Controller page", e);
			scene = null;
		}
		return scene;
	}

	@FXML
	private TableView<MakeupIndicator> addMeetingList(TableView<MakeupIndicator> meetingListTable1) {

		List<MakeupIndicator> list = new ArrayList<>();
		list.add(new MakeupIndicator(Constants.USER_NAME, new Date(), new Date()));
		list.add(new MakeupIndicator(Constants.USER_NAME2, new Date(), new Date()));
		ObservableList<MakeupIndicator> items = FXCollections.observableList(list);
		TableColumn<MakeupIndicator, Date> date = new TableColumn<>("Date");
		date.setCellValueFactory(new PropertyValueFactory<>("meetingDate"));
		date.setMinWidth(180);
		TableColumn<MakeupIndicator, Date> time = new TableColumn<>("Time");
		time.setCellValueFactory(new PropertyValueFactory<>("meetingTime"));
		time.setMinWidth(90);
		TableColumn<MakeupIndicator, String> user = new TableColumn<>("User");
		user.setCellValueFactory(new PropertyValueFactory<>("userName"));
		user.setMinWidth(90);
		TableColumn<MakeupIndicator, Hyperlink> meetingLink = new TableColumn<>("Meeting Link");
		meetingLink.setCellFactory(
				new Callback<TableColumn<MakeupIndicator, Hyperlink>, TableCell<MakeupIndicator, Hyperlink>>() {

					@Override
					public TableCell call(TableColumn arg0) {

						return new ButtonCell();
					}
				});
		meetingLink.setMinWidth(80);
		meetingListTable1.autosize();
		meetingListTable1.setFixedCellSize(80);
		meetingListTable1.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		meetingListTable1.setItems(items);
		meetingListTable1.getColumns().addAll(date, time, meetingLink);
		return meetingListTable1;
	}

	public class ButtonCell extends TableCell<MakeupIndicator, Hyperlink> {

		ButtonCell() {
			joinMeeting = new Hyperlink();
			if (joinMeeting != null) {
				joinMeeting.setOnAction(event -> {
					Node node1 = (Node) event.getSource();
					setScene(node1.getScene(),null,null);
				});

			} else {
				log.info("DEBUG::Button did not get initiated in inner class ");
			}

		}

		@Override
		protected void updateItem(Hyperlink arg0, boolean arg1) {

			super.updateItem(arg0, arg1);
			if (!arg1) {
				joinMeeting.setText("Start Meeting ");
				setGraphic(joinMeeting);
			} else {
				setGraphic(null);
			}

		}
	}

	/**
	 * For now this is inner class can make this as seperate class and can
	 * resuse this for all Table cell details.
	 */

	@Override
	public void setScene(Scene scene,BonjourRequest request,BonjourResponse response) {
		this.scene = scene;
		log.info("DEBUG::Moving ahead to next scene as navigation initiated");
		scene.getWindow().hide();
		Controller controller = BonjourControllerFactory.getController("MakeupDetailedController");
		if (controller != null) {
			BonjourRunner runner = new BonjourRunner();
			runner.execute(controller,null,null);
		} else {
			log.info("INFO::Controller Object is null");
		}

		log.info("INFO::Controller Scene is not avaialable");
	}

	@FXML
	public void setRequestedMeeting(ActionEvent event) {
		scene = ((Node) event.getSource()).getScene();
		setScene(scene,null,null);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
}
