package com.ysl.bonjour.controller;

import java.awt.Desktop;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import com.ysl.bonjour.bean.BonjourMakeupRequest;
import com.ysl.bonjour.bean.BonjourMakeupResponse;
import com.ysl.bonjour.bean.BonjourRequest;
import com.ysl.bonjour.bean.BonjourResponse;
import com.ysl.bonjour.core.BonjourRunner;
import com.ysl.bonjour.util.Constants;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * @author pku134 Service class will have connectivity with other classes
 */

public class ServiceController implements Controller,Initializable {

	@FXML private Button hangout;
	@FXML private Button captureFirst;
	@FXML private Button captureLater;
	@FXML private ImageView imageViewLater;
	@FXML private Button emailId;
	@FXML private ImageView imageViewFirst;
	@FXML private TextField product1;
	@FXML private TextField product2;
	@FXML private TextField tip1;
	@FXML private TextField tip2;
	@FXML private TextField tip3;
	@FXML private Button finish;
	
	private Logger log = Logger.getLogger(ServiceController.class);
	private Scene scene;
	private Robot robot;
	private Rectangle rectangle;
	private BorderPane pane;
	private static BonjourMakeupResponse response;
	private static BonjourMakeupRequest request;

	@Override
	public Scene getScene(BonjourRequest request,BonjourResponse response) {
		try {
			log.info("INFO::In Controller of Service ");
			setRequest((BonjourMakeupRequest)request);
			setResponse((BonjourMakeupResponse)response);
			FXMLLoader fxmlLoader=new FXMLLoader(ServiceController.class.getClassLoader().getResource(Constants.SERVICE_FXML_PATH));
			pane = (BorderPane) fxmlLoader.load();
			if (pane != null) {
				pane.setBackground(new Background(new BackgroundFill(Color.BLACK, new CornerRadii(10), new Insets(5))));
				scene = new Scene(pane);
				scene.setRoot(pane);
				return scene;
			} else {
				log.error("ERROR::An error due to receival of null pane");
			}
		} catch (IOException e) {
			log.error("ERROR::Error occured in getting hangout page", e);
			scene = null;
		}
		return scene;
	}

	/**
	 * Sets scene navigation and for future scene enabling between navigation
	 */
	@Override
	public void setScene(Scene scene,BonjourRequest request,BonjourResponse response) {
		this.scene = scene;
		log.info("DEBUG::Moving ahead to next scene as navigation initiated");
		scene.getWindow().hide();
		Controller controller = BonjourControllerFactory.getController("SummaryController");
		if (controller != null) {
			BonjourRunner runner = new BonjourRunner();
			runner.execute(controller,request,response);
		} else {
			log.info("INFO::Controller Object is null");
		}
	}

	@FXML
	public void setHangout(ActionEvent event) {
		try {
			URI hangoutURL = new URI(Constants.HANGOUT_URL);
			Desktop desktop = Desktop.getDesktop();
			desktop.browse(hangoutURL);
		} catch (URISyntaxException e) {
			log.error("ERROR::Error occured in getting hangout page", e);
		} catch (Exception e) {
			log.error("ERROR::Error occured in getting hangout page", e);
		}
	}

	@FXML
	public void setCapture(ActionEvent event) {
		try {
			log.info("INFO::Capture Intiated");
			Node source = (Node) event.getSource();
			Stage stage = (Stage) source.getScene().getWindow();
			stage.hide();
			Thread.sleep(50);
			rectangle = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
			robot = new Robot();

			BufferedImage bufferedImage = robot.createScreenCapture(rectangle);
			if (bufferedImage != null) {
				WritableImage image = new WritableImage(bufferedImage.getWidth(), bufferedImage.getHeight());
				PixelWriter pw = image.getPixelWriter();
				for (int i = 0; i < bufferedImage.getWidth(); i++) {
					for (int j = 0; j < bufferedImage.getHeight(); j++) {
						pw.setArgb(i, j, bufferedImage.getRGB(i, j));
					}
				}
				String buttonName = ((Button) event.getSource()).getId();
				if ("captureFirst".equals(buttonName)) {
					boolean imageStatus = ImageIO.write(SwingFXUtils.fromFXImage(image, bufferedImage),
							Constants.CAPTURED_IMAGE_TYPE, new File(Constants.CAPTURED_IMAGE_PATH));
					log.debug("DEBUG::Image Save status was " + imageStatus);
					log.info("INFO::Image Saved on to disk at" + new File(Constants.CAPTURED_IMAGE_PATH).getPath());
					imageViewFirst.setImage(image);
					imageViewFirst.getImage();
					captureFirst.setDisable(true);
				} else if ("captureLater".equals(buttonName)) {
					boolean imageStatus = ImageIO.write(SwingFXUtils.fromFXImage(image, bufferedImage),
							Constants.CAPTURED_IMAGE_TYPE, new File(Constants.CAPTUREDLATER_IMAGE_PATH));
					log.debug("DEBUG::Image Save status was " + imageStatus);
					log.info("INFO::Image Saved on to disk at" + new File(Constants.CAPTUREDLATER_IMAGE_PATH).getPath());
					imageViewLater.setImage(image);
					imageViewLater.getImage();
					captureLater.setDisable(true);
				}
				stage.show();
			} else {
				log.info("INFO::Could not capture image please capture image after some time");
			}
		} catch (Exception e) {
			log.error("ERROR::Error occured in capturing screen", e);
		}
	}

	@FXML
	public void setFinish(ActionEvent event)
	{
		this.request=getRequest();
		Node node=(Node)event.getSource();
		this.response=getResponse();
		response.setCaptureFirst(Constants.CAPTURED_IMAGE_PATH);
		response.setCaptureLater(Constants.CAPTUREDLATER_IMAGE_PATH);
		response.setProduct1(product1.getText());
		response.setProduct2(product2.getText());
		response.setTip1(tip1.getText());
		response.setTip2(tip2.getText());
		response.setTip3(tip3.getText());	
		setScene(node.getScene(),request,response);
	}
	
	public BonjourMakeupRequest getRequest() {
		return request;
	}

	public void setRequest(BonjourMakeupRequest request) {
		this.request = request;
	}

	public BonjourMakeupResponse getResponse() {
		return response;
	}

	public void setResponse(BonjourMakeupResponse response) {
		this.response = response;
		
	}
	@FXML
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
	}
	
}
