package com.ysl.bonjour.controller;

import com.ysl.bonjour.bean.BonjourRequest;
import com.ysl.bonjour.bean.BonjourResponse;

import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 * @author pku134
 *
 *This is a controller interface which leads to get controller objects and fetches and pushes javafx scene.
 */
public interface Controller extends Initializable {

	/*
	 * This interface can have methods for future purpose on sending request and receiving response objects.
	 */
	public Object getScene(BonjourRequest request,BonjourResponse response);
	public void setScene(Scene scene,BonjourRequest request,BonjourResponse response);

}
