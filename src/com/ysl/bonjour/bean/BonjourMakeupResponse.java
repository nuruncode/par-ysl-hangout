package com.ysl.bonjour.bean;

/**
 * @author pku134
 *
 *This class is to pass as response object from required classes. 
 */
public class BonjourMakeupResponse implements BonjourResponse
{
	private String captureFirst;
	private String captureLater;
	private String emailID;
	private String product1;
	private String product2;
	private String product3;
	private String tip1;
	private String tip2;
	private String tip3;
	
	public String getCaptureFirst() {
		return captureFirst;
	}
	public void setCaptureFirst(String captureFirst) {
		this.captureFirst = captureFirst;
	}
	public String getCaptureLater() {
		return captureLater;
	}
	public void setCaptureLater(String captureLater) {
		this.captureLater = captureLater;
	}
	public String getEmailID() {
		return emailID;
	}
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	public String getProduct1() {
		return product1;
	}
	public void setProduct1(String product1) {
		this.product1 = product1;
	}
	public String getProduct2() {
		return product2;
	}
	public void setProduct2(String product2) {
		this.product2 = product2;
	}
	public String getProduct3() {
		return product3;
	}
	public void setProduct3(String product3) {
		this.product3 = product3;
	}
	public String getTip1() {
		return tip1;
	}
	public void setTip1(String tip1) {
		this.tip1 = tip1;
	}
	public String getTip2() {
		return tip2;
	}
	public void setTip2(String tip2) {
		this.tip2 = tip2;
	}
	public String getTip3() {
		return tip3;
	}
	public void setTip3(String tip3) {
		this.tip3 = tip3;
	}
}
